class Traveler {
    constructor (name){
    this._name = name
    this._food = 1
    this._isHealthy = true
    }
    get name() {
        return this._name
    }
    set name(name){
        this._name = name
    }
    get food(){
        return this._food
    }
    set food(newFood) {
        this._food = newFood
    }
    get isHealthy(){
        return this._isHealthy
    }
    set isHealthy(newisHealth) {
        this._isHealthy = newisHealth
    }


    hunt() {
      this._food+=2
    }
    eat() {
        if (this._food > 0){
            this._food--
        }else{
            this._isHealthy = false
        }
    }
}

class Wagon {
    constructor (capacity){
        this._capacity = capacity
        this._passengers = []
    }

    get capacity() {
        return this._capacity
    }
    set capacity(capacity){
        this._capacity = capacity
    }
    get passengers() {
        return this._passengers
    }
    set passengers(newPassengers){
        this._passengers = newPassengers
    }

    
    getAvailableSeatCount() {
        return this.capacity - this.passengers.length
    }
    join(traveler) {
        if (this.getAvailableSeatCount() > 0){
            this.passengers.push(traveler)
        }
    }
    shouldQuarantine() {
        let output = false
        for (let i=0; i < this.passengers.length; i++){
            if (this.passengers[i].isHealthy === false) {
                output = true
            }
        }
        return output
    }
    totalFood() {
        let allFood = 0
        for (let i=0; i < this.passengers.length; i++){
        allFood += this.passengers[i].food
        }
        return allFood
    }
}

// Criar uma carroça que comporta 2 pessoas
let wagon = new Wagon(2);
// Criar três viajantes
let henrietta = new Traveler('Henrietta');
let juan = new Traveler('Juan');
let maude = new Traveler('Maude');

console.log(`${wagon.getAvailableSeatCount()} should be 2`);

wagon.join(henrietta);
console.log(`${wagon.getAvailableSeatCount()} should be 1`);

wagon.join(juan);
wagon.join(maude); // Não tem espaço para ela!
console.log(`${wagon.getAvailableSeatCount()} should be 0`);

henrietta.hunt(); // pega mais comida
juan.eat();
juan.eat(); // juan agora está com fome (doente)

console.log(`${wagon.shouldQuarantine()} should be true since juan is sick`);
console.log(`${wagon.totalFood()} should be 3`);